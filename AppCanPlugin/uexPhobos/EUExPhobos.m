//
//  EUExDemo.m
//  EUExDemo
//
//  Created by LiXianyu on 14-06-30.
//  Copyright (c) 2014年 LiXianyu. All rights reserved.
//

#import "EUExPhobos.h"
#import "EUtility.h"
#import "PhobosAdder.h"

// Defines for the TI CC2540 keyfob peripheral
#define TI_KEYFOB_PROXIMITY_ALERT_UUID                      0x1802
#define TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID             0x2a06
#define TI_KEYFOB_PROXIMITY_ALERT_ON_VAL                    0x01
#define TI_KEYFOB_PROXIMITY_ALERT_OFF_VAL                   0x00
#define TI_KEYFOB_PROXIMITY_ALERT_WRITE_LEN                 1

#define TI_KEYFOB_LINK_LOSS_SERVICE_UUID                    0x1803
#define TI_KEYFOB_LINK_LOSS_PROPERTY_UUID                   0x2a06
#define TI_KEYFOB_LINK_LOSS_WRITE_LEN                       1


#define TI_KEYFOB_PROXIMITY_TX_PWR_SERVICE_UUID             0x1804
#define TI_KEYFOB_PROXIMITY_TX_PWR_NOTIFICATION_UUID        0x2A07
#define TI_KEYFOB_PROXIMITY_TX_PWR_NOTIFICATION_READ_LEN    1

#define TI_KEYFOB_BATT_SERVICE_UUID                         0x180F
#define TI_KEYFOB_LEVEL_SERVICE_UUID                        0x2A19
#define TI_KEYFOB_LEVEL_SERVICE_READ_LEN                    1

//#define TI_KEYFOB_ACCEL_SERVICE_UUID                        0xFFA0
//#define TI_KEYFOB_ACCEL_ENABLER_UUID                        0xFFA1
//#define TI_KEYFOB_ACCEL_RANGE_UUID                          0xFFA2
//#define TI_KEYFOB_ACCEL_READ_LEN                            1
//#define TI_KEYFOB_ACCEL_X_UUID                              0xFFA3
//#define TI_KEYFOB_ACCEL_Y_UUID                              0xFFA4
//#define TI_KEYFOB_ACCEL_Z_UUID                              0xFFA5
//
//#define TI_KEYFOB_KEYS_SERVICE_UUID                         0xFFE0
//#define TI_KEYFOB_KEYS_NOTIFICATION_UUID                    0xFFE1
//#define TI_KEYFOB_KEYS_NOTIFICATION_READ_LEN                1

#if defined(GET_TEMPERATURE_USE_BATTERY_SERVICE)
#define TI_KEYFOB_TEMPERATURE_SERVICE_UUID                  0x180F
#define TI_KEYFOB_TEMPERATURE_CHARACTERISTIC                0x2A1C
#else
#define TI_KEYFOB_TEMPERATURE_SERVICE_UUID                  0xFFB0
#define TI_KEYFOB_TEMPERATURE_CHARACTERISTIC                0xFFB1
#endif
#define TI_KEYFOB_TEMPERATURE_CHARACTERISTIC_READ_LEN       4

typedef NS_ENUM(NSInteger, BLEState){
    BLEStateAdd = 0, //添加新的PHOBOS
    BLEStatePairing,
    BLEStateFind
};

typedef NS_ENUM(NSInteger, CommunicateState){
    CommunicateStateIdle = 0,
    CommunicateStateBuzzer,
    CommunicateStateTemperature,
    CommunicateStateBattery,
    CommunicateStateLinkLoss
};

@interface EUExPhobos() <CBCentralManagerDelegate, CBPeripheralDelegate>
@property (retain, nonatomic) PhobosAdder *phobosAdder;
@property (retain, nonatomic) CBCentralManager *bleManager;
@property (assign, nonatomic) BLEState bleState;

//For add new phobos.

//@property (retain, nonatomic) NSMutableDictionary *everFoundPeripheral;
//@property (retain, nonatomic) NSMutableDictionary *unknowBLEs;
//@property (retain, nonatomic) NSMutableArray *uuids;
//@property (retain, nonatomic) NSString *curUUID;

//For find phobos.
@property (nonatomic) int scanCount;
@property (retain, nonatomic) NSMutableDictionary *everConnectedUUID; //所有曾经已连接过的
@property (retain, nonatomic) NSArray *knownPeripherals;
@property (retain, nonatomic) NSMutableDictionary *knownPeripheralsDict;
@property (retain, nonatomic) CBPeripheral *p;


@property (assign, nonatomic) CommunicateState communicateState;
@property (retain, nonatomic) NSString *buzzerUUID;
@property (retain, nonatomic) NSString *linkLossUUID;
@property (retain, nonatomic) NSString *batteryUUID;
@property (retain, nonatomic) CBService *buzzerService;
@property (retain, nonatomic) CBService *linkLossService;
@property (retain, nonatomic) CBService *batteryService;
@property (nonatomic) float batteryLevel;
@end

@implementation EUExPhobos {
    Byte linkLossValue;
}

#pragma mark - Init
-(id)initWithBrwView:(EBrowserView *)eInBrwView{
    NSLog(@"%s", __func__);
    self = [super initWithBrwView:eInBrwView];
    if (self) {
        if (_bleManager == nil) {
            _bleManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
            NSLog(@"_bleManager = %@", _bleManager);
        }
        self.everConnectedUUID = [NSMutableDictionary dictionaryWithCapacity:5];
        NSLog(@"everConnectedUUID = %@", self.everConnectedUUID);
        NSLog(@"_scanCount = %d", _scanCount);
        _bleState = BLEStateFind;
    }
    return self;
}

#pragma mark - For Add And Pair New Phobos.
- (void)initAddNewPhobos:(NSMutableArray *)array {
    NSLog(@"%s, _bleManager = %@", __func__, _bleManager);
//    _bleState = BLEStateAdd;
//    self.foundPeripheral = nil;
    if (self.phobosAdder == nil) {
        self.phobosAdder = [[PhobosAdder alloc] init];
        [self.phobosAdder initWithEverConnectedUUID:self];
    }
//    [self initEverConnectedUUID];
}

//开始寻找新的Phobos
//找到后会调用回调uexPhobos.didDiscoverPeripheral(uuid)
- (void)scanNewPhobos:(NSMutableArray *)array {
    NSLog(@"%s", __func__);
//    [self scanPeripherals:YES];
    [self.phobosAdder scanPeripherals];
}

//配对成功后，会调用回调uexPhobos.didFinishPairOK()
//配对失败后，会调用回调uexPhobos.didFinishPairFail()
- (void)pairNewPhobos:(NSMutableArray *)array {
    NSLog(@"%s", __func__);
    NSString *uuid = [array objectAtIndex:0];
//    NSString *uuid = @"853C5886-90EA-7499-847B-96A29C28D2C8";
//    [_bleManager connectPeripheral:self.foundPeripheral[uuid] options:nil];
    [self.phobosAdder pairIt:uuid];
}

// 配对完后需调用此函数关闭
- (void)closeAddNewPhobos:(NSMutableArray*)array {
    NSLog(@"%s", __func__);
    [self.phobosAdder closeMe];
    self.phobosAdder = nil;
}

#pragma mark - For Find Phobos (Connect already paired Phobos)
/*在开始连接指定UUID的Phobos之前，需要调用此函数
 *只需调用一次这个函数，之后可直接调用findPhobos了
 */
- (void)initFindPhobos:(NSMutableArray *)array {
    NSLog(@"%s, _bleManager = %@", __func__, _bleManager);
    _bleState = BLEStateFind;
    [self initEverConnectedUUID];
}

/* 开始连接指定UUID的Phobos
 */
- (void)findPhobos:(NSMutableArray*)array {
    NSLog(@"%s", __func__);
    //NSString *uuid = [[array objectAtIndex:0] stringValue];
//    NSString *uuid = @"853C5886-90EA-7499-847B-96A29C28D2C8";
    NSString *uuid = [array objectAtIndex:0];
    NSLog(@"uuid = %@", uuid);
    [self connectPhobos:uuid];
}

// 断开指定UUID
- (void)cancelConnectPhobos:(NSMutableArray*)array {
    NSLog(@"%s", __func__);
    NSString *uuid = [array objectAtIndex:0];
    //    NSString *uuid = @"853C5886-90EA-7499-847B-96A29C28D2C8";
    CBPeripheral *perihperal = self.knownPeripheralsDict[uuid];
    if (perihperal) {
        [_bleManager cancelPeripheralConnection:perihperal];
    }
}

- (void)deleteOnePhobos:(NSMutableArray*)array {
    NSLog(@"%s", __func__);
    NSString *uuid = [array objectAtIndex:0];
    [self deletePhobosWithUuid:uuid];
}

/* 读取一个指定Phobos的电池电量
 *
 */
- (void)readTheBattery:(NSMutableArray*)array {
    NSLog(@"%s", __func__);
//    NSString *uuid = @"853C5886-90EA-7499-847B-96A29C28D2C8";
    NSString *uuid = [array objectAtIndex:0];
    [self readBattery:uuid];
}

- (void)soundThePhobos:(NSMutableArray*)array {
    NSLog(@"%s", __func__);
//    NSString *uuid = @"853C5886-90EA-7499-847B-96A29C28D2C8";
    NSString *uuid = [array objectAtIndex:0];
    [self soundBuzzer:uuid];
}

- (void)readTheRSSI:(NSMutableArray*)array {
    NSLog(@"%s", __func__);
//    NSString *uuid = @"853C5886-90EA-7499-847B-96A29C28D2C8";
    NSString *uuid = [array objectAtIndex:0];
    CBPeripheral *peripheral = self.knownPeripheralsDict[uuid];
    [peripheral readRSSI];
}

#if 0
-(void)open:(NSMutableArray *)array{
    NSLog(@"%s", __func__);
    if ([array isKindOfClass:[NSMutableArray class]] && [array count]>0) {
        CGFloat x = [[array objectAtIndex:0] floatValue];
        CGFloat y = [[array objectAtIndex:1] floatValue];
        CGFloat w = [[array objectAtIndex:2] floatValue];
        CGFloat h = [[array objectAtIndex:3] floatValue];
        if (!view) {
            view = [[UIView alloc] initWithFrame:CGRectMake(x, y, w, h)];
            [view setBackgroundColor:[UIColor whiteColor]];
            [EUtility brwView:meBrwView addSubview:view];
        }
        
        //读取本插件的资源文件
        {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, 140, 50)];
            [label setFont:[UIFont systemFontOfSize:15]];
            [label setBackgroundColor:[UIColor redColor]];
            [label setText:@"本地插件资源文件："];
            [view addSubview:label];
            [label release];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(150, 50, 100, 100)];
            NSString *path = [[NSBundle mainBundle] pathForResource:@"uexDemo/plugin_uexDemo_BG" ofType:@"png"];
            UIImage *image = [UIImage imageWithContentsOfFile:path];
            [imageView setImage:image];
            [view addSubview:imageView];
            [imageView release];
        }
        
        //读取协议路径资源文件 例如res://
        {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 200, 140, 50)];
            [label setText:@"协议路径资源文件："];
            [label setFont:[UIFont systemFontOfSize:15]];
            [label setBackgroundColor:[UIColor redColor]];
            [view addSubview:label];
            [label release];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(150, 200, 100, 100)];
            NSString *path = @"res://plugin_uexDemo_RES.png";
            path = [EUtility getAbsPath:self.meBrwView path:path];
            UIImage *image = [UIImage imageWithContentsOfFile:path];
            [imageView setImage:image];
            [view addSubview:imageView];
            [imageView release];
        }
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(110, 350, 100, 50)];
        [btn setBackgroundColor:[UIColor redColor]];
        [btn setTitle:@"回掉测试" forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(uexDemoBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:btn];
        [btn release];
    }
}

//回调函数
-(void)uexDemoBtnClicked:(UIButton *)btn{
    NSLog(@"%s", __func__);
    [self.meBrwView stringByEvaluatingJavaScriptFromString:@"uexDemo.CallBack();"];
}

-(void)close:(NSMutableArray *)array{
    NSLog(@"%s", __func__);
    if (view) {
        [view removeFromSuperview];
        if (self.view) {
            self.view = nil;
        }
    }
}

//当前窗口调用uexWindow.close()接口的时候 插件的clean方法会被调用
-(void)clean{
    NSLog(@"%s", __func__);
}
#endif

#pragma mark - Some util
- (void)callBackHere:(NSString*)command {
    [self performSelector:@selector(callBackDo:) withObject:command afterDelay:.1f];
}

- (void)callBackDo:(NSString*)command {
    [self.meBrwView stringByEvaluatingJavaScriptFromString:command];
}

- (void)scanPeripherals:(BOOL)flag {
    NSLog(@"%s", __func__);
    if (flag) {
        NSDictionary *optionsdict = @{CBCentralManagerScanOptionAllowDuplicatesKey : [NSNumber numberWithBool:YES]};
        [_bleManager scanForPeripheralsWithServices:nil options:optionsdict];
    }
    else { //支持程序后台扫描
        [_bleManager scanForPeripheralsWithServices:nil options:nil];
        _scanCount++;
    }
//  NSArray *arrayUUID = [NSArray arrayWithObjects:[CBUUID UUIDWithString:@"853C5886-90EA-7499-847B-96A29C28D2C8"],
//                                      [CBUUID UUIDWithString:@"1802"], nil];
//  [_bleManager scanForPeripheralsWithServices:arrayUUID options:nil];
}

- (void)stopScanPeripherals {
    _scanCount--;
    if (_scanCount <= 0) {
        _scanCount = 0;
        [_bleManager stopScan];
    }
}

- (void)connectPhobos:(NSString *)uuid {
    NSLog(@"%s, uuid = %@", __func__, uuid);
    CBPeripheral *peripheral = self.knownPeripheralsDict[uuid];
    if ([peripheral state] == CBPeripheralStateDisconnected) {
        [self doConnectPhobos:peripheral];
        [self scanPeripherals:NO];
    }
    else {
        NSString *jsString = [NSString stringWithFormat:@"uexPhobos.didFindIt(\"%@\");", uuid];
        NSLog(@" jsString is %@", jsString);
        [self callBackHere:jsString];
    }
}

- (void)doConnectPhobos:(CBPeripheral*)aPeripheral {
    NSLog(@"%s, uuid = %@", __func__, aPeripheral.identifier.UUIDString);
//    aPeripheral.delegate = self;
//    NSDictionary *connectOptions = @{CBConnectPeripheralOptionNotifyOnConnectionKey : [NSNumber numberWithBool:NO],
//                                     CBConnectPeripheralOptionNotifyOnDisconnectionKey : [NSNumber numberWithBool:NO],
//                                     CBConnectPeripheralOptionNotifyOnNotificationKey : [NSNumber numberWithBool:YES]};
    [_bleManager connectPeripheral:aPeripheral options:nil];
}

- (void)initEverConnectedUUID {
    NSLog(@"%s", __func__);
    NSString *filePath = [self dataFilePath];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSArray *array = [[NSArray alloc] initWithContentsOfFile:filePath];
        for (NSString *uuid in array) {
            if ([self.everConnectedUUID isKindOfClass:[NSMutableDictionary class]]) {
                [self.everConnectedUUID setObject:uuid forKey:[[NSUUID alloc] initWithUUIDString:uuid]];
            }
        }
    }
    self.knownPeripherals = [_bleManager retrievePeripheralsWithIdentifiers:[self.everConnectedUUID allKeys]];
    NSLog(@"%s: self.knownPeripherals = %@", __func__, self.knownPeripherals);
    self.knownPeripheralsDict = [NSMutableDictionary dictionaryWithCapacity:5];
    
    for (CBPeripheral *aPeripheral in self.knownPeripherals) {
        [self.knownPeripheralsDict setObject:aPeripheral forKey:aPeripheral.identifier.UUIDString];
        aPeripheral.delegate = self;
    }
    NSLog(@"%s: self.knownPeripheralsDict = %@", __func__, self.knownPeripheralsDict);
}

// 当配对成功后需要调用这个函数保存一下
- (void)savePeripheral:(CBPeripheral*)peripheral {
    NSLog(@"%s", __func__);
    NSString *filePath = [self dataFilePath];
    NSArray *array = nil;
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSLog(@"OH, dataPhobosEver.plist file exist!");
        array = [[NSArray alloc] initWithContentsOfFile:filePath];
    }
    else {
        NSLog(@"OH, dataPhobosEver.plist file does not exist!");
    }
    NSMutableArray *uuidString = [NSMutableArray arrayWithArray:array];
    [uuidString addObject:peripheral.identifier.UUIDString];
    BOOL ret = [uuidString writeToFile:filePath atomically:YES];
    NSLog(@"ret = %@", ret?@"YES":@"NO");
    
    
//    [self.everConnectedUUID setObject:peripheral.identifier.UUIDString forKey:[[NSUUID alloc] initWithUUIDString:peripheral.identifier.UUIDString]];
//    self.knownPeripherals = [_bleManager retrievePeripheralsWithIdentifiers:[self.everConnectedUUID allKeys]];
//    NSLog(@"%s: self.knownPeripherals = %@", __func__, self.knownPeripherals);
//    self.knownPeripheralsDict = [NSMutableDictionary dictionaryWithCapacity:5];
//    for (CBPeripheral *aPeripheral in self.knownPeripherals) {
//        [self.knownPeripheralsDict setObject:aPeripheral forKey:aPeripheral.identifier.UUIDString];
//        aPeripheral.delegate = self;
//    }
}

- (void)deletePhobosWithUuid:(NSString*)uuid {
    NSLog(@"%s", __func__);
    NSString *filePath = [self dataFilePath];
    NSArray *array = nil;
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSLog(@"OH, dataPhobosEver.plist file exist!");
        array = [[NSArray alloc] initWithContentsOfFile:filePath];
    }
    else {
        NSLog(@"OH, dataPhobosEver.plist file does not exist! so just return.");
        return;
    }
    NSMutableArray *uuidString = [NSMutableArray arrayWithArray:array];
    [uuidString removeObject:uuid];
    BOOL ret = [uuidString writeToFile:filePath atomically:YES];
    NSLog(@"ret = %@", ret?@"YES":@"NO");
    NSString *jsString = [NSString stringWithFormat:@"uexPhobos.didFinishedDelete(\"%@\");", uuid];
    NSLog(@" jsString is %@", jsString);
    [self callBackHere:jsString];
}

- (NSString *)dataFilePath {
    NSLog(@"%s", __func__);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:@"dataPhobosEver.plist"];
}

- (NSInteger)getNewPhobosCounter {
    NSLog(@"%s", __func__);
    NSNumber *oldNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"newPhobosCounter"];
    if (oldNumber == nil) {
        [self setNewPhobosCounter:1];
        return 0;
    }
    NSInteger counter = [oldNumber integerValue];
    [self setNewPhobosCounter:counter+1];
    return counter;
}

- (void)setNewPhobosCounter:(NSInteger)number {
    NSLog(@"%s: number = %d", __func__, number);
    [[NSUserDefaults standardUserDefaults] setInteger:number forKey:@"newPhobosCounter"];
}

#if 0
//在配对新Phobos时候，才会走到这里
- (void)saveIt:(CBPeripheral*)peripheral {
    //    NSLog(@"_everConnectedUUID.count = %d", everConnectedUUID.count);
    NSLog(@"%s, peripheral = %@", __func__, peripheral);
    NSLog(@"self.foundPeripheral=%@", self.foundPeripheral);
    
    //    [_allConnectPeripheral setObject:peripheral forKey:peripheral.identifier.UUIDString];
    //    [everConnectedUUID setObject:peripheral.identifier.UUIDString forKey:peripheral.identifier];
    
    //    NSLog(@"everConnectedUUID = %@", everConnectedUUID);
    NSMutableArray *uuidString = [NSMutableArray arrayWithCapacity:5];
    //    for (NSUUID *aNSUUID in everConnectedUUID) {
    //        [uuidString addObject:aNSUUID.UUIDString];
    //    }
    for (NSString *uuid in [self.foundPeripheral allKeys]) {
        [uuidString addObject:uuid];
    }
    BOOL ret = [uuidString writeToFile:[self dataFilePath] atomically:YES];
    NSLog(@"ret = %@", ret?@"YES":@"NO");
}
#endif

- (void)printAdvLog:(NSDictionary*)advertisementData {
    NSArray *keys = [advertisementData allKeys];
    NSData *dataAmb, *dataObj;
    for (int i = 0; i < [keys count]; ++i) {
        id key = [keys objectAtIndex: i];
        NSString *keyName = (NSString *) key;
        NSObject *value = [advertisementData objectForKey: key];
        if ([value isKindOfClass: [NSArray class]]) {
            printf("   key: %s\n", [keyName cStringUsingEncoding: NSUTF8StringEncoding]);
            NSArray *values = (NSArray *) value;
            for (int j = 0; j < [values count]; ++j) {
                if ([[values objectAtIndex: j] isKindOfClass: [CBUUID class]]) {
                    CBUUID *uuid = [values objectAtIndex: j];
                    NSData *data = uuid.data;
                    if (j == 0) {
                        dataObj = uuid.data;
                    } else {
                        dataAmb = uuid.data;
                    }
                    printf("      uuid(%d):", j);
                    for (int j = 0; j < data.length; ++j)
                        printf(" %02X", ((UInt8 *) data.bytes)[j]);
                    printf("\n");
                } else {
                    const char *valueString = [[value description] cStringUsingEncoding: NSUTF8StringEncoding];
                    printf("      value(%d): %s\n", j, valueString);
                }
            }
        } else {
            const char *valueString = [[value description] cStringUsingEncoding: NSUTF8StringEncoding];
            printf("   key: %s, value: %s\n", [keyName cStringUsingEncoding: NSUTF8StringEncoding], valueString);
        }
    }
}

#pragma mark - CBCentralManagerDelegate
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    NSLog(@"%s", __func__);
    NSLog(@"state = %d", central.state);
    switch (central.state) {
        case CBCentralManagerStatePoweredOn: {
            NSLog(@"CBCentralManagerStatePoweredOn");
            break;
        }
        case CBCentralManagerStatePoweredOff:
            NSLog(@"CBCentralManagerStatePoweredOff");
            break;
        case CBCentralManagerStateResetting:
            NSLog(@"CBCentralManagerStateResetting");
            break;
        case CBCentralManagerStateUnauthorized:
            NSLog(@"CBCentralManagerStateUnauthorized");
            break;
        case CBCentralManagerStateUnknown:
            NSLog(@"CBCentralManagerStateUnknown");
            break;
        case CBCentralManagerStateUnsupported:
            NSLog(@"CBCentralManagerStateUnsupported");
            break;
        default:
            break;
    }
}

//- (void)centralManager:(CBCentralManager *)central willRestoreState:(NSDictionary *)dict {
//    NSLog(@"%s", __func__);
//}

- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals {
    NSLog(@"%s", __func__);
}

- (void)centralManager:(CBCentralManager *)central didRetrieveConnectedPeripherals:(NSArray *)peripherals {
    NSLog(@"%s", __func__);
}

- (void)handleDiscoverPeripheralInFindState:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    NSLog(@"%s: self.knownPeripheralsDict = %@", __func__, self.knownPeripheralsDict);
    if (self.knownPeripheralsDict[peripheral.identifier.UUIDString]) {
        NSLog(@"oh my god........");
        [self doConnectPhobos:peripheral];
    }
//    if ([peripheral.identifier.UUIDString isEqualToString:_curUUID]) {
//        NSLog(@"oh my god........");
//        [self doConnectPhobos:peripheral];
//    }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)      advertisementData RSSI:(NSNumber *)RSSI {
    NSLog(@"Did discover peripheral. peripheral: %@, rssi: %@, UUID: %@, advertisementData: %@", peripheral, RSSI, peripheral.identifier.UUIDString, advertisementData);
    
    if (_bleState == BLEStateFind) {
        [self handleDiscoverPeripheralInFindState:central didDiscoverPeripheral:peripheral advertisementData:advertisementData RSSI:RSSI];
        return;
    }
    
    [self printAdvLog:advertisementData];

    
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s: peripheral = %@", __func__, peripheral);
    if (_bleState == BLEStateAdd) {
//        NSLog(@"self.foundPeripheral = %@", self.foundPeripheral);
//        NSArray *sUUID = @[[CBUUID UUIDWithString:@"013d8e3b-1877-4d5c-bc59-aaa7e5082346"]];
//        [peripheral discoverServices:sUUID];
    }
    else {
        [self stopScanPeripherals];
        NSString *jsString = [NSString stringWithFormat:@"uexPhobos.didFindIt(\"%@\");", peripheral.identifier.UUIDString];
        NSLog(@" jsString is %@", jsString);
        [self callBackHere:jsString];
    }
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
    if (_bleState == BLEStateAdd) {
//        [self callBackHere:@"uexPhobos.didFinishPairFail();"];
    }
    else {
        [self stopScanPeripherals];
        NSString *jsString = [NSString stringWithFormat:@"uexPhobos.didFailToFindIt(\"%@\");", peripheral.identifier.UUIDString];
        NSLog(@" jsString is %@", jsString);
        [self callBackHere:jsString];
    }
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
    NSString *jsString = [NSString stringWithFormat:@"uexPhobos.didDisconnectPeripheral(\"%@\");", peripheral.identifier.UUIDString];
    NSLog(@" jsString is %@", jsString);
    [self callBackHere:jsString];
}

#pragma mark - CBPeripheralDelegate
- (void)peripheralDidUpdateName:(CBPeripheral *)peripheral {//NS_AVAILABLE(NA, 6_0);
    NSLog(@"%s", __func__);
}

- (void)peripheralDidInvalidateServices:(CBPeripheral *)peripheral {//NS_DEPRECATED(NA, NA, 6_0, 7_0);
    NSLog(@"%s", __func__);
}

- (void)peripheral:(CBPeripheral *)peripheral didModifyServices:(NSArray *)invalidatedServices {//NS_AVAILABLE(NA, 7_0);
    NSLog(@"%s", __func__);
}

- (void)peripheralDidUpdateRSSI:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
    if (error) {
        return;
    }
    if (peripheral.RSSI == nil) {
        return;
    }
    NSLog(@"RSSI = %@", peripheral.RSSI);
    int rssi = [peripheral.RSSI intValue];
    NSString *jsString = [NSString stringWithFormat:@"uexPhobos.peripheralDidUpdateRSSI(\"%@\", \"%d\");", peripheral.identifier.UUIDString, rssi];
    NSLog(@" jsString is %@", jsString);
    [self callBackHere:jsString];
}

// 发现了Service
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
    if (_bleState == BLEStateAdd) {
//        for (CBService *s in peripheral.services) {
//            NSLog(@"Service found : %@",s.UUID);
//        //        if ([[[s.UUID UUIDString] lowercaseString] isEqualToString:@"013d8e3b-1877-4d5c-bc59-aaa7e5082346"]) {
//        //            NSLog(@"Really found!");
//        //        }
//            NSArray *cUUID = @[[CBUUID UUIDWithString:@"51567b6d-a035-499d-a2ea-1e27b5ae8b37"]];
//            [peripheral discoverCharacteristics:cUUID forService:s];
//        }
    } else {
        NSLog(@"%s, _communicateState = %d", __func__, _communicateState);
        CBUUID *aCBUUID;
        if (_communicateState == CommunicateStateBuzzer) {
            aCBUUID = [CBUUID UUIDWithString:@"1802"];
        } else if (_communicateState == CommunicateStateLinkLoss) {
            aCBUUID = [CBUUID UUIDWithString:@"1803"];
        } else if (_communicateState == CommunicateStateBattery) {
            aCBUUID = [CBUUID UUIDWithString:@"180f"];
        } else if (_communicateState == CommunicateStateTemperature) {
            aCBUUID = [CBUUID UUIDWithString:@"ffb0"];
        }
        for (CBService *service in peripheral.services) {
            NSLog(@"service = %@", service);
            if (1 == [self compareCBUUID:service.UUID UUID2:aCBUUID]) {
                NSLog(@"Yes, got it.");
                if (_communicateState == CommunicateStateBuzzer) {
                    NSLog(@"Yes, got 1802.");
                    _buzzerService = service;
                    [self performSelector:@selector(soundBuzzer2) withObject:nil afterDelay:0.1];
                } else if (_communicateState == CommunicateStateLinkLoss) {
                    NSLog(@"Yes, got 1803.");
                    _linkLossService = service;
                    [self performSelector:@selector(setLinkLoss2) withObject:nil afterDelay:0.1];
                } else if (_communicateState == CommunicateStateBattery) {
                    NSLog(@"Yes, got 180f.");
                    _batteryService = service;
                    [self performSelector:@selector(readBattery2:) withObject:peripheral afterDelay:0.1];
                } else if (_communicateState == CommunicateStateTemperature) {
                    NSLog(@"Yes, got 180f -- temperature.");
//                    _temperatureService = service;
                    [self performSelector:@selector(readTemperature2) withObject:nil afterDelay:0.1];
                }
                return;
            }
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverIncludedServicesForService:(CBService *)service error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

// 发现了Characteristic
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
    if (_bleState == BLEStateAdd) {
//        for ( CBCharacteristic *characteristic in service.characteristics ) {
//            NSLog(@"characteristic.properties = 0x%x", characteristic.properties);
//            [peripheral readValueForCharacteristic:characteristic];
//        }
    } else {
        if (_communicateState == CommunicateStateBuzzer) {
            [self performSelector:@selector(soundBuzzer3) withObject:nil afterDelay:0.1];
        } else if (_communicateState == CommunicateStateLinkLoss) {
            [self performSelector:@selector(setLinkLoss3) withObject:nil afterDelay:0.1];
        } else if (_communicateState == CommunicateStateBattery) {
            [self performSelector:@selector(readBattery3:) withObject:peripheral afterDelay:0.1];
        } else if (_communicateState == CommunicateStateTemperature) {
            [self performSelector:@selector(readTemperature3) withObject:nil afterDelay:0.1];
        }
    }
}

// If something had read.
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
    if (_bleState == BLEStateFind) {
        goto FindState;
    }
//    if (!error) {
//        //配对成功
//#if 1
//        [self savePeripheral:peripheral];
//#else
////        [self saveIt:peripheral];
//        [self performSelector:@selector(saveIt:) withObject:peripheral afterDelay:0.1];
//#endif
//        [self callBackHere:@"uexPhobos.didFinishPairOK();"];
//    }
//    else {
//        [self callBackHere:@"uexPhobos.didFinishPairFail();"];
//    }
//    return;
FindState:
    NSLog(@"haha");
    UInt16 characteristicUUID = [self CBUUIDToInt:characteristic.UUID];
    if (!error) {
        switch (characteristicUUID) {
            case TI_KEYFOB_LEVEL_SERVICE_UUID: {
                char batlevel;
                [characteristic.value getBytes:&batlevel length:TI_KEYFOB_LEVEL_SERVICE_READ_LEN];
                _batteryLevel = (float)batlevel;
                NSLog(@"_batteryLevel = %f", _batteryLevel);
                NSString *jsString = [NSString stringWithFormat:@"uexPhobos.didGotBattery(\"%@\", \"%f\");", peripheral.identifier.UUIDString,_batteryLevel];
                NSLog(@" jsString is %@", jsString);
                [self callBackHere:jsString];
                break;
            }
                
//            case TI_KEYFOB_TEMPERATURE_CHARACTERISTIC: {
//                SInt32 tempLevel;
//                [characteristic.value getBytes:&tempLevel length:TI_KEYFOB_TEMPERATURE_CHARACTERISTIC_READ_LEN];
//                float tempHight, tempLow;
//                tempHight = (float)(tempLevel >> 16);
//                tempLow = (float)(tempLevel & 0x0000FFFF);
//                NSLog(@"tempHight = %f, tempLow = %f", tempHight, tempLow);
//                _tempLevel = tempHight + (tempLow/10);
//                NSLog(@"temperature = %f", _tempLevel);
//                for (id targetDelegate in _delegate) {
//                    if ([targetDelegate respondsToSelector:@selector(didUpdateTemperature:)]) {
//                        [targetDelegate didUpdateTemperature:_tempLevel];
//                    }
//                }
//                break;
//            }
            default:
                break;
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

#pragma mark - Read&Write
/*
 *  @method compareCBUUID
 *
 *  @param UUID1 UUID 1 to compare
 *  @param UUID2 UUID 2 to compare
 *
 *  @returns 1 (equal) 0 (not equal)
 *
 *  @discussion compareCBUUID compares two CBUUID's to each other and returns 1 if they are equal and 0 if they are not
 *
 */

-(int) compareCBUUID:(CBUUID *)UUID1 UUID2:(CBUUID *)UUID2 {
    char b1[16];
    char b2[16];
    [UUID1.data getBytes:b1];
    [UUID2.data getBytes:b2];
    if (memcmp(b1, b2, UUID1.data.length) == 0) return 1;
    else return 0;
}

/*
 *  @method CBUUIDToInt
 *
 *  @param UUID1 UUID 1 to convert
 *
 *  @returns UInt16 representation of the CBUUID
 *
 *  @discussion CBUUIDToInt converts a CBUUID to a Uint16 representation of the UUID
 *
 */
-(UInt16) CBUUIDToInt:(CBUUID *) UUID {
    char b1[16];
    [UUID.data getBytes:b1];
    return ((b1[0] << 8) | b1[1]);
}

/*!
 *  @method swap:
 *
 *  @param s Uint16 value to byteswap
 *
 *  @discussion swap byteswaps a UInt16
 *
 *  @return Byteswapped UInt16
 */

-(UInt16) swap:(UInt16)s {
    UInt16 temp = s << 8;
    temp |= (s >> 8);
    return temp;
}

/*
 *  @method CBUUIDToString
 *
 *  @param UUID UUID to convert to string
 *
 *  @returns Pointer to a character buffer containing UUID in string representation
 *
 *  @discussion CBUUIDToString converts the data of a CBUUID class to a character pointer for easy printout using printf()
 *
 */
-(const char *) CBUUIDToString:(CBUUID *) UUID {
    return [[UUID.data description] cStringUsingEncoding:NSStringEncodingConversionAllowLossy];
}


/*
 *  @method UUIDToString
 *
 *  @param UUID UUID to convert to string
 *
 *  @returns Pointer to a character buffer containing UUID in string representation
 *
 *  @discussion UUIDToString converts the data of a CFUUIDRef class to a character pointer for easy printout using printf()
 *
 */
-(const char *) UUIDToString:(CFUUIDRef)UUID {
    if (!UUID) return "NULL";
    CFStringRef s = CFUUIDCreateString(NULL, UUID);
    return CFStringGetCStringPtr(s, 0);
    
}

/*
 *  @method findServiceFromUUID:
 *
 *  @param UUID CBUUID to find in service list
 *  @param p Peripheral to find service on
 *
 *  @return pointer to CBService if found, nil if not
 *
 *  @discussion findServiceFromUUID searches through the services list of a peripheral to find a
 *  service with a specific UUID
 *
 */
-(CBService *) findServiceFromUUID:(CBUUID *)UUID p:(CBPeripheral *)p {
    for(int i = 0; i < p.services.count; i++) {
        CBService *s = [p.services objectAtIndex:i];
        if ([self compareCBUUID:s.UUID UUID2:UUID]) return s;
    }
    return nil; //Service not found on this peripheral
}

/*
 *  @method findCharacteristicFromUUID:
 *
 *  @param UUID CBUUID to find in Characteristic list of service
 *  @param service Pointer to CBService to search for charateristics on
 *
 *  @return pointer to CBCharacteristic if found, nil if not
 *
 *  @discussion findCharacteristicFromUUID searches through the characteristic list of a given service
 *  to find a characteristic with a specific UUID
 *
 */
-(CBCharacteristic *) findCharacteristicFromUUID:(CBUUID *)UUID service:(CBService*)service {
    for(int i=0; i < service.characteristics.count; i++) {
        CBCharacteristic *c = [service.characteristics objectAtIndex:i];
        if ([self compareCBUUID:c.UUID UUID2:UUID]) return c;
    }
    return nil; //Characteristic not found on this service
}

/*!
 *  @method writeValue:
 *
 *  @param serviceUUID Service UUID to write to (e.g. 0x2400)
 *  @param characteristicUUID Characteristic UUID to write to (e.g. 0x2401)
 *  @param data Data to write to peripheral
 *  @param p CBPeripheral to write to
 *  @param flag If true, then write with CBCharacteristicWriteWithResponse
 *
 *  @discussion Main routine for writeValue request, writes without feedback. It converts integer into
 *  CBUUID's used by CoreBluetooth. It then searches through the peripherals services to find a
 *  suitable service, it then checks that there is a suitable characteristic on this service.
 *  If this is found, value is written. If not nothing is done.
 *
 */

- (NSInteger)writeValue:(int)serviceUUID characteristicUUID:(int)characteristicUUID p:(CBPeripheral *)p data:(NSData *)data Response:(BOOL)flag{
    UInt16 s = [self swap:serviceUUID];
    UInt16 c = [self swap:characteristicUUID];
    NSData *sd = [[NSData alloc] initWithBytes:(char *)&s length:2];
    NSData *cd = [[NSData alloc] initWithBytes:(char *)&c length:2];
    CBUUID *su = [CBUUID UUIDWithData:sd];
    CBUUID *cu = [CBUUID UUIDWithData:cd];
    CBService *service = [self findServiceFromUUID:su p:p];
    if (!service) {
        NSLog(@"Could not find service with UUID %s on peripheral with UUID %@\r\n",(char*)[self CBUUIDToString:su],p.identifier.UUIDString);
        return 1;
    }
    CBCharacteristic *characteristic = [self findCharacteristicFromUUID:cu service:service];
    if (!characteristic) {
        NSLog(@"Could not find characteristic with UUID %s on service with UUID %s on peripheral with UUID %@\r\n", (char*)[self CBUUIDToString:cu],[self CBUUIDToString:su], p.identifier.UUIDString);
        return 2;
    }
    if (flag) {
        [p writeValue:data forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
    } else {
        [p writeValue:data forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
    }
    return 0;
}


/*!
 *  @method readValue:
 *
 *  @param serviceUUID Service UUID to read from (e.g. 0x2400)
 *  @param characteristicUUID Characteristic UUID to read from (e.g. 0x2401)
 *  @param p CBPeripheral to read from
 *
 *  @discussion Main routine for read value request. It converts integers into
 *  CBUUID's used by CoreBluetooth. It then searches through the peripherals services to find a
 *  suitable service, it then checks that there is a suitable characteristic on this service.
 *  If this is found, the read value is started. When value is read the didUpdateValueForCharacteristic
 *  routine is called.
 *
 *  @see didUpdateValueForCharacteristic
 */

- (NSInteger)readValue:(int)serviceUUID characteristicUUID:(int)characteristicUUID p:(CBPeripheral *)p {
    UInt16 s = [self swap:serviceUUID];
    UInt16 c = [self swap:characteristicUUID];
    NSData *sd = [[NSData alloc] initWithBytes:(char *)&s length:2];
    NSData *cd = [[NSData alloc] initWithBytes:(char *)&c length:2];
    CBUUID *su = [CBUUID UUIDWithData:sd];
    CBUUID *cu = [CBUUID UUIDWithData:cd];
    CBService *service = [self findServiceFromUUID:su p:p];
    if (!service) {
        printf("Could not find service with UUID %s on peripheral with UUID %s\r\n",[self CBUUIDToString:su],[self UUIDToString:p.UUID]);
        return 1;
    }
    CBCharacteristic *characteristic = [self findCharacteristicFromUUID:cu service:service];
    if (!characteristic) {
        printf("Could not find characteristic with UUID %s on service with UUID %s on peripheral with UUID %s\r\n",[self CBUUIDToString:cu],[self CBUUIDToString:su], [self UUIDToString:p.UUID]);
        return 2;
    }
    
    //    CBMutableCharacteristic *rCharacteristic = [[CBMutableCharacteristic alloc] initWithType:characteristic.UUID properties:CBCharacteristicPropertyRead|CBCharacteristicPropertyNotifyEncryptionRequired value:characteristic.value permissions:CBAttributePermissionsReadEncryptionRequired];
    
    //    [p readValueForCharacteristic:rCharacteristic];
    
    [p readValueForCharacteristic:characteristic];
    return 0;
}

/*!
 *  @method soundBuzzer:
 *
 *  @param buzVal The data to write
 *  @param p CBPeripheral to write to
 *
 *  @discussion Sound the buzzer on a TI keyfob. This method writes a value to the proximity alert service
 *
 */
- (void)soundBuzzer:(Byte)buzVal peripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s", __func__);
    NSData *d = [[NSData alloc] initWithBytes:&buzVal length:TI_KEYFOB_PROXIMITY_ALERT_WRITE_LEN];
    [self writeValue:TI_KEYFOB_PROXIMITY_ALERT_UUID characteristicUUID:TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID p:peripheral data:d Response:NO];
    [self performSelector:@selector(stopBuzzer) withObject:nil afterDelay:10];
}

- (void)soundBuzzer:(NSString *)uuid {
    NSLog(@"%s: uuid = %@", __func__, uuid);
    _communicateState = CommunicateStateBuzzer;
    CBPeripheral *peripheral = self.knownPeripheralsDict[uuid];
    self.buzzerUUID = uuid;
    NSArray *sUUID = @[[CBUUID UUIDWithString:@"1802"]];
    [peripheral discoverServices:sUUID];
}

- (void)soundBuzzer2 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = self.knownPeripheralsDict[self.buzzerUUID];
    [peripheral discoverCharacteristics:nil forService:_buzzerService];
}

- (void)soundBuzzer3 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = self.knownPeripheralsDict[self.buzzerUUID];
    [self soundBuzzer:0x02 peripheral:peripheral];
}

- (void)stopBuzzer {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = self.knownPeripheralsDict[self.buzzerUUID];
    Byte buzVal = 0x00;
    NSData *data = [[NSData alloc] initWithBytes:&buzVal length:TI_KEYFOB_PROXIMITY_ALERT_WRITE_LEN];
    [self writeValue:TI_KEYFOB_PROXIMITY_ALERT_UUID characteristicUUID:TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID p:peripheral data:data Response:NO];
}

//-------------------------------------------------------------------------------------------------------------------
- (void)setLinkLoss:(Byte)buzVal peripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s", __func__);
    NSData *d = [[NSData alloc] initWithBytes:&buzVal length:TI_KEYFOB_LINK_LOSS_WRITE_LEN];
    [self writeValue:TI_KEYFOB_LINK_LOSS_SERVICE_UUID characteristicUUID:TI_KEYFOB_LINK_LOSS_PROPERTY_UUID p:peripheral data:d Response:YES];
}

/** 0:都提醒
 * 1:仅电话提醒
 * 2:仅Phobos提醒
 */
- (void)setLinkLoss:(NSString *)uuid value:(Byte)buzVal {
    NSLog(@"%s: uuid = %@, buzVal = %d", __func__, uuid, buzVal);
    _communicateState = CommunicateStateLinkLoss;
    CBPeripheral *peripheral = self.knownPeripheralsDict[uuid];
    _linkLossUUID = uuid;
    NSData *d = [[NSData alloc] initWithBytes:&buzVal length:TI_KEYFOB_LINK_LOSS_WRITE_LEN];
    NSInteger ret = [self writeValue:TI_KEYFOB_LINK_LOSS_SERVICE_UUID characteristicUUID:TI_KEYFOB_LINK_LOSS_PROPERTY_UUID p:peripheral data:d Response:YES];
    NSLog(@"ret = %d", ret);
    if (ret == 0) {
        return;
    }
    linkLossValue = buzVal;
    NSArray *sUUID = @[[CBUUID UUIDWithString:@"1803"]];
    [peripheral discoverServices:sUUID];
}

- (void)setLinkLoss2 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = self.knownPeripheralsDict[_linkLossUUID];
    [peripheral discoverCharacteristics:nil forService:_linkLossService];
}

- (void)setLinkLoss3 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = self.knownPeripheralsDict[_linkLossUUID];
    [self setLinkLoss:linkLossValue peripheral:peripheral];
}

//-------------------------------------------------------------------------------------------------------------------
/*!
 *  @method readBattery:
 *
 *  @param p CBPeripheral to read from
 *
 *  @discussion Start a battery level read cycle from the battery level service
 *
 */
- (void)readBatteryPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s", __func__);
    [self readValue:TI_KEYFOB_BATT_SERVICE_UUID characteristicUUID:TI_KEYFOB_LEVEL_SERVICE_UUID p:peripheral];
}

- (void)readBattery:(NSString*)uuid {
    NSLog(@"%s", __func__);
    _communicateState = CommunicateStateBattery;
    CBPeripheral *peripheral = self.knownPeripheralsDict[uuid];
    self.batteryUUID = uuid;
    NSInteger retValue = [self readValue:TI_KEYFOB_BATT_SERVICE_UUID characteristicUUID:TI_KEYFOB_LEVEL_SERVICE_UUID p:peripheral];
    NSLog(@"retValue = %d", retValue);
    if (retValue == 0) {
        return;
    }
    
    NSArray *sUUID = @[[CBUUID UUIDWithString:@"180f"]];
    [peripheral discoverServices:sUUID];
}

#if 0
- (void)readBattery2:(NSString*)uuid {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _everFoundPeripheral[self.batteryUUID];
    [peripheral discoverCharacteristics:nil forService:_batteryService];
}
#else
- (void)readBattery2:(CBPeripheral*)peripheral {
    NSLog(@"%s", __func__);
    [peripheral discoverCharacteristics:nil forService:_batteryService];
}
#endif

#if 0
- (void)readBattery3 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _everFoundPeripheral[self.batteryUUID];
    [self readBatteryPeripheral:peripheral];
}
#else
- (void)readBattery3:(CBPeripheral*)peripheral {
    NSLog(@"%s", __func__);
    //CBPeripheral *peripheral = _everFoundPeripheral[self.batteryUUID];
    [self readBatteryPeripheral:peripheral];
}
#endif
@end
