//
//  PhobosAdder.m
//  AppCanPlugin
//
//  Created by Li Xianyu on 14-7-3.
//  Copyright (c) 2014年 zywx. All rights reserved.
//

#import <CoreBluetooth/CoreBluetooth.h>
#import "PhobosAdder.h"


@interface PhobosAdder() <CBCentralManagerDelegate, CBPeripheralDelegate>
@property (retain, nonatomic) EUExPhobos *euexPhobos;
@property (retain, nonatomic) NSMutableDictionary *foundPeripheral;
@property (retain, nonatomic) CBCentralManager *bleManager;
@property (retain, nonatomic) NSArray *knownPeripherals;
@property (retain, nonatomic) NSMutableDictionary *knownPeripheralsDict;
@end

@implementation PhobosAdder

- (void)initWithEverConnectedUUID:(EUExPhobos*)euexPhobos {
    NSLog(@"%s", __func__);
    self.euexPhobos = euexPhobos;
    
    NSString *filePath = [self dataFilePath];
    NSMutableDictionary *everConnectedUUID = [NSMutableDictionary dictionaryWithCapacity:3];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSArray *array = [[NSArray alloc] initWithContentsOfFile:filePath];
        for (NSString *uuid in array) {
            [everConnectedUUID setObject:uuid forKey:[[NSUUID alloc] initWithUUIDString:uuid]];
        }
    }
    if (_bleManager == nil) {
        _bleManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
        NSLog(@"_bleManager = %@", _bleManager);
    }
    self.foundPeripheral = nil;
    self.knownPeripherals = [_bleManager retrievePeripheralsWithIdentifiers:[everConnectedUUID allKeys]];
    NSLog(@"%s: knownPeripherals = %@", __func__, self.knownPeripherals);
    self.knownPeripheralsDict = [NSMutableDictionary dictionaryWithCapacity:5];
    
    for (CBPeripheral *aPeripheral in self.knownPeripherals) {
        [self.knownPeripheralsDict setObject:aPeripheral forKey:aPeripheral.identifier.UUIDString];
        aPeripheral.delegate = self;
    }
    NSLog(@"%s: self.knownPeripheralsDict = %@", __func__, self.knownPeripheralsDict);
}

- (NSString *)dataFilePath {
    NSLog(@"%s", __func__);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:@"dataPhobosEver.plist"];
}

- (void)scanPeripherals {
    NSLog(@"%s", __func__);

    NSDictionary *optionsdict = @{CBCentralManagerScanOptionAllowDuplicatesKey : [NSNumber numberWithBool:YES]};
    [_bleManager scanForPeripheralsWithServices:nil options:optionsdict];
    //  NSArray *arrayUUID = [NSArray arrayWithObjects:[CBUUID UUIDWithString:@"853C5886-90EA-7499-847B-96A29C28D2C8"],
    //                                      [CBUUID UUIDWithString:@"1802"], nil];
    //  [_bleManager scanForPeripheralsWithServices:arrayUUID options:nil];
}

- (void)pairIt:(NSString*)uuid {
    NSLog(@"%s", __func__);
    [_bleManager connectPeripheral:self.foundPeripheral[uuid] options:nil];
}

- (void)closeMe {
    NSLog(@"%s", __func__);
    if (_bleManager) {
        [_bleManager stopScan];
//        _bleManager = nil;
    }
}

#pragma mark - CBCentralManagerDelegate
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    NSLog(@"%s", __func__);
    NSLog(@"state = %d", central.state);
    switch (central.state) {
        case CBCentralManagerStatePoweredOn: {
            NSLog(@"CBCentralManagerStatePoweredOn");
            break;
        }
        case CBCentralManagerStatePoweredOff:
            NSLog(@"CBCentralManagerStatePoweredOff");
            break;
        case CBCentralManagerStateResetting:
            NSLog(@"CBCentralManagerStateResetting");
            break;
        case CBCentralManagerStateUnauthorized:
            NSLog(@"CBCentralManagerStateUnauthorized");
            break;
        case CBCentralManagerStateUnknown:
            NSLog(@"CBCentralManagerStateUnknown");
            break;
        case CBCentralManagerStateUnsupported:
            NSLog(@"CBCentralManagerStateUnsupported");
            break;
        default:
            break;
    }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)      advertisementData RSSI:(NSNumber *)RSSI {
    NSLog(@"Did discover peripheral. peripheral: %@, rssi: %@, UUID: %@, advertisementData: %@", peripheral, RSSI, peripheral.identifier.UUIDString, advertisementData);
    int iRssi = [RSSI intValue];
    NSLog(@"iRssi = %d", iRssi);
    // 只有当手机离Phobos很近的时候，才会识别。
    // 这样做是为了避免周围有很多Phobos，对配对产生混乱。
    if (iRssi < -37 || iRssi > 0) {
        return;
    }
    NSString *localName = advertisementData[@"kCBAdvDataLocalName"];
    NSLog(@"localName = %@", localName);
    if (![localName isEqualToString:@"Phobos"]) {
        return;
    }
    if ([self.knownPeripherals containsObject:peripheral]) {
        NSLog(@"Already ever added the it: %@, so just return.", peripheral.identifier.UUIDString);
        return;
    }
    if (self.foundPeripheral == nil) {
        self.foundPeripheral = [[NSMutableDictionary alloc] initWithCapacity:2];
    }
    
    if ([self.foundPeripheral objectForKey:peripheral.identifier.UUIDString] == nil) {
        [central stopScan];
        NSLog(@"哦，发现了一个新的待配对的Phobos！");
        [self.foundPeripheral setObject:peripheral forKey:peripheral.identifier.UUIDString];
        peripheral.delegate = self;
        NSString *jsString = [NSString stringWithFormat:@"uexPhobos.didDiscoverPeripheral(\"%@\");", peripheral.identifier.UUIDString];
        NSLog(@" jsString is %@", jsString);
        [self.euexPhobos callBackHere:jsString];
    }
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s: peripheral = %@", __func__, peripheral);
    NSLog(@"self.foundPeripheral = %@", self.foundPeripheral);
    NSArray *sUUID = @[[CBUUID UUIDWithString:@"013d8e3b-1877-4d5c-bc59-aaa7e5082346"]];
    [peripheral discoverServices:sUUID];
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
    NSString *jsString = [NSString stringWithFormat:@"uexPhobos.didFinishPairFail(\"%d\");", 1];
    NSLog(@" jsString is %@", jsString);
    [self.euexPhobos callBackHere:jsString];
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
    NSString *jsString = [NSString stringWithFormat:@"uexPhobos.didFinishPairFail(\"%d\");", 2];
    NSLog(@" jsString is %@", jsString);
    [self.euexPhobos callBackHere:jsString];
}

#pragma mark - CBPeripheralDelegate
// 发现了Service
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
    for (CBService *s in peripheral.services) {
        NSLog(@"Service found : %@",s.UUID);
        NSArray *cUUID = @[[CBUUID UUIDWithString:@"51567b6d-a035-499d-a2ea-1e27b5ae8b37"]];
        [peripheral discoverCharacteristics:cUUID forService:s];
    }
}

// 发现了Characteristic
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
    for ( CBCharacteristic *characteristic in service.characteristics ) {
        NSLog(@"characteristic.properties = 0x%x", characteristic.properties);
        [peripheral readValueForCharacteristic:characteristic];
    }
}

// If something had read.
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
    if (!error) {
        //配对成功
        [self.euexPhobos savePeripheral:peripheral];
        [self.euexPhobos callBackHere:@"uexPhobos.didFinishPairOK();"];
    }
    else {
        NSString *jsString = [NSString stringWithFormat:@"uexPhobos.didFinishPairFail(\"%d\");", 0];
        NSLog(@" jsString is %@", jsString);
        [self.euexPhobos callBackHere:jsString];
    }
    return;
}
@end
