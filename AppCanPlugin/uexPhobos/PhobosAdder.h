//
//  PhobosAdder.h
//  AppCanPlugin
//
//  Created by Li Xianyu on 14-7-3.
//  Copyright (c) 2014年 zywx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EUExPhobos.h"

@interface PhobosAdder : NSObject

- (void)initWithEverConnectedUUID:(EUExPhobos*)euexPhobos;
- (void)scanPeripherals;
- (void)pairIt:(NSString*)uuid; 
- (void)closeMe;
@end
