//
//  WidgetOnePseudoDelegate.m
//  WBPalm
//
//  Created by 邹 达 on 11-12-15.
//  Copyright 2011 zywx. All rights reserved.
//

#import "WidgetOnePseudoDelegate.h"

@implementation WidgetOnePseudoDelegate

- (id) init
{	
	if (self = [super init]) {
		self.userStartReport = NO;
	}
	return self;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	return [super application:application didFinishLaunchingWithOptions:launchOptions];
}
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url 
{
    return [super application:application handleOpenURL:url];
}
- (void)applicationWillResignActive:(UIApplication *)application {
	[super applicationWillResignActive:application];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	[super applicationDidBecomeActive:application];
}

- (void)applicationWillTerminate:(UIApplication *)application {
	[super applicationWillTerminate:application];
}
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
	[super application:app didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
	
}
// 注册APNs错误

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
	[super application:app didFailToRegisterForRemoteNotificationsWithError:err];
	
}
// 接收推送通知

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
	[super application:application didReceiveRemoteNotification:userInfo];
}

- (void)dealloc
{
	[super dealloc];
}
//-(void)terminateWithException:(NSException*)e{
//    [super terminateWithException:e];
//}
@end
